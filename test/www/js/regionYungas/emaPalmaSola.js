var palmaSola = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
palmaSola.open('GET', 'http://api.wunderground.com/api/24350adefbeb2b9d/conditions/q/pws:IJUJUYPA2.json', true);

palmaSola.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
palmaSola.onload = function() {
	var palmaSolaInfo = JSON.parse(palmaSola.responseText);
	document.getElementById('currentTemp5').innerHTML = palmaSolaInfo.current_observation.temp_c;
	document.getElementById('humedad5').innerHTML = palmaSolaInfo.current_observation.relative_humidity;
	document.getElementById('icono5').src = palmaSolaInfo.current_observation.icon_url;
}