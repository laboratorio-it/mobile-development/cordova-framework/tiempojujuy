var sanPedro = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
sanPedro.open('GET', 'http://api.wunderground.com/api/f577e42a17aabb02/conditions/q/pws:IJUJUYPR2.json', true);

sanPedro.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
sanPedro.onload = function() {
	var sanPedroInfo = JSON.parse(sanPedro.responseText);
	document.getElementById('currentTemp4').innerHTML = sanPedroInfo.current_observation.temp_c;
	document.getElementById('humedad4').innerHTML = sanPedroInfo.current_observation.relative_humidity;
	document.getElementById('icono4').src = sanPedroInfo.current_observation.icon_url;
}