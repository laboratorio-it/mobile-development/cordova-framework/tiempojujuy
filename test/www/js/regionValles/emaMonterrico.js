var monterrico = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
monterrico.open('GET', 'http://api.wunderground.com/api/da9972c7aa341e74/conditions/q/pws:IJUJUYMO3.json', true);

monterrico.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
monterrico.onload = function() {
	var monterricoInfo = JSON.parse(monterrico.responseText);
	document.getElementById('currentTemp7').innerHTML = monterricoInfo.current_observation.temp_c;
	document.getElementById('humedad7').innerHTML = monterricoInfo.current_observation.relative_humidity;
	document.getElementById('icono7').src = monterricoInfo.current_observation.icon_url;
}