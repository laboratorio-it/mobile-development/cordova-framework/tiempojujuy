var perico = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
perico.open('GET', 'http://api.wunderground.com/api/a0b40f1f6ba579f3/conditions/q/pws:IJUJUYPE4.json', true);

perico.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
perico.onload = function() {
	var pericoInfo = JSON.parse(perico.responseText);
	document.getElementById('currentTemp6').innerHTML = pericoInfo.current_observation.temp_c;
	document.getElementById('humedad6').innerHTML = pericoInfo.current_observation.relative_humidity;
	document.getElementById('icono6').src = pericoInfo.current_observation.icon_url;
}
