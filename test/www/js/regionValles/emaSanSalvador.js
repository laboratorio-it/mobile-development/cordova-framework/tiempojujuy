var weatherObject = new XMLHttpRequest()

//Obtener datos de la api de wunderground según el ID de la EMA
weatherObject.open('GET', 'http://api.wunderground.com/api/d8b7ac53c0f71e22/conditions/q/pws:IJUJUYPR4.json', true);

weatherObject.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
weatherObject.onload = function() {
	var weatherInfo = JSON.parse(weatherObject.responseText);
	document.getElementById('currentTemp').innerHTML = weatherInfo.current_observation.temp_c;
	document.getElementById('humedad').innerHTML = weatherInfo.current_observation.relative_humidity;
	document.getElementById('icono').src = weatherInfo.current_observation.icon_url;
}
