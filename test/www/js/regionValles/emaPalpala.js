var palpala = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
palpala.open('GET', 'http://api.wunderground.com/api/c53b1d4229bccdc8/conditions/q/pws:IJUJUYCA2.json', true);

palpala.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
palpala.onload = function() {
	var palpalaInfo = JSON.parse(palpala.responseText);
	document.getElementById('currentTemp2').innerHTML = palpalaInfo.current_observation.temp_c;
	document.getElementById('humedad2').innerHTML = palpalaInfo.current_observation.relative_humidity;
	document.getElementById('icono2').src = palpalaInfo.current_observation.icon_url;
}
