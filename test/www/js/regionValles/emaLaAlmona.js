var laAlmona = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
laAlmona.open('GET', 'http://api.wunderground.com/api/5c279e7506b312bc/conditions/q/pws:IJUJUYSA4.json', true);

laAlmona.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
laAlmona.onload = function() {
	var laAlmonaInfo = JSON.parse(laAlmona.responseText);
	document.getElementById('currentTemp1').innerHTML = laAlmonaInfo.current_observation.temp_c;
	document.getElementById('humedad1').innerHTML = laAlmonaInfo.current_observation.relative_humidity;
	document.getElementById('icono1').src = laAlmonaInfo.current_observation.icon_url;
}
