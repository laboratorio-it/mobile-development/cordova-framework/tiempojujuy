var elCarmen = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
elCarmen.open('GET', 'http://api.wunderground.com/api/40e4fad324352694/conditions/q/pws:IJUJUYEL3.json', true);

elCarmen.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
elCarmen.onload = function() {
	var elCarmenInfo = JSON.parse(elCarmen.responseText);
	document.getElementById('currentTemp3').innerHTML = elCarmenInfo.current_observation.temp_c;
	document.getElementById('humedad3').innerHTML = elCarmenInfo.current_observation.relative_humidity;
	document.getElementById('icono3').src = elCarmenInfo.current_observation.icon_url;
}
