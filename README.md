# Tiempo en Jujuy [Deprecated]

[Página web - Tiempo en Jujuy](http://www.tiempoenjujuy.com)

## Información Importante
Debido a la funcionalidad y expansión de la aplicación.
El equipo de Tiempo en Jujuy decidio que unicamente va a estar disponible
para uso interno.
Más información comunicarse con Tiempo en Jujuy.

## Descripción

La aplicación tiene por objetivo informar el estado del tiempo de toda la provincia de Jujuy.
A través de la ubicación del usuario se puede obtener automáticamente el estado actual del tiempo, 
de acuerdo a la Estación Meteorológica Automática (EMA) más cercana a la ubicación obtenida.

## Plataformas Soportadas
- Android
- BlackBerry10 mediante apk

## Instalación
Clonar el repositorio
```
git clone https://gitlab.com/laboratorio-it/mobile-development/cordova-framework/tiempojujuy.git
```

Una vez clonado el repositorio dirigirse a la carpeta
```
cd tiempojujuy/test
```

Desde ahí ejecutar el script
```
npm run tejscript
```

Para ejecutar la aplicación en el navegador
```
cordova run browser
```

Para ejecutar la aplicación en un dispositivo Android
```
cordova run android
```
